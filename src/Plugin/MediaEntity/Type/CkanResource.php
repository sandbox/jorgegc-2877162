<?php

namespace Drupal\media_entity_ckan\Plugin\MediaEntity\Type;

use Drupal\ckan_connect\Parser\CkanResourceUrlParserInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media_entity\EmbedCodeValueTrait;
use Drupal\media_entity\MediaInterface;
use Drupal\media_entity\MediaTypeBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides media type plugin for CKAN Resource.
 *
 * @MediaType(
 *   id = "ckan_resource",
 *   label = @Translation("CKAN Resource"),
 *   description = @Translation("Provides business logic and metadata for CKAN Resource.")
 * )
 */
class CkanResource extends MediaTypeBase {

  use EmbedCodeValueTrait;

  /**
   * The CKAN resource URL parser.
   *
   * @var \Drupal\ckan_connect\Parser\CkanResourceUrlParserInterface
   */
  protected $ckanResourceUrlParser;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Config\Config $config
   *   Media entity config object.
   * @param \Drupal\ckan_connect\Parser\CkanResourceUrlParserInterface $ckan_resource_url_parser
   *   The CKAN resource URL parser.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, Config $config, CkanResourceUrlParserInterface $ckan_resource_url_parser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $config);
    $this->ckanResourceUrlParser = $ckan_resource_url_parser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('config.factory')->get('media_entity.settings'),
      $container->get('ckan_connect.resource_url_parser')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function providedFields() {
    return [
      'package_id' => $this->t('Package ID'),
      'resource_id' => $this->t('Resource ID'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getField(MediaInterface $media, $name) {
    $field = FALSE;
    $url = $this->getMediaUrl($media);

    switch ($name) {
      case 'package_id':
        $field = $this->ckanResourceUrlParser->getPackageId($url);
        break;

      case 'resource_id':
        $field = $this->ckanResourceUrlParser->getResourceId($url);
        break;
    }

    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function thumbnail(MediaInterface $media) {
    return $this->config->get('icon_base') . '/generic.png';
  }

  /**
   * {@inheritdoc}
   */
  public function attachConstraints(MediaInterface $media) {
    parent::attachConstraints($media);

    if (!empty($this->configuration['source_field'])) {
      $source_field_name = $this->configuration['source_field'];

      if ($media->hasField($source_field_name)) {
        foreach ($media->get($source_field_name) as &$embed_code) {
          /** @var \Drupal\Core\TypedData\DataDefinitionInterface $typed_data */
          $typed_data = $embed_code->getDataDefinition();
          $typed_data->addConstraint('CkanResourceAccessible');
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\media_entity\MediaBundleInterface $media_bundle */
    $media_bundle = $form_state->getFormObject()->getEntity();
    $allowed_field_types = ['link', 'string', 'string_long'];
    $source_field_options = [];

    foreach ($this->entityFieldManager->getFieldDefinitions('media', $media_bundle->id()) as $field_name => $field) {
      if (in_array($field->getType(), $allowed_field_types) && !$field->getFieldStorageDefinition()->isBaseField()) {
        $source_field_options[$field_name] = $field->getLabel();
      }
    }

    $form['source_field'] = array(
      '#type' => 'select',
      '#title' => $this->t('Field with source information'),
      '#description' => $this->t('Field on media entity that stores CKAN URL. You can create a bundle without selecting a value for this dropdown initially. This dropdown can be populated after adding fields to the bundle.'),
      '#default_value' => empty($this->configuration['source_field']) ? NULL : $this->configuration['source_field'],
      '#options' => $source_field_options,
    );

    return $form;
  }

  /**
   * Gets the URL from the 'source_field'.
   *
   * @param \Drupal\media_entity\MediaInterface $media
   *  The media entity.
   *
   * @return string|bool
   *  The URL 'source_field' if found, FALSE otherwise.
   */
  protected function getMediaUrl(MediaInterface $media) {
    $url = FALSE;

    if (isset($this->configuration['source_field'])) {
      $source_field = $this->configuration['source_field'];

      if ($media->hasField($source_field) && !$media->get($source_field)->isEmpty()) {
        $value = $media->get($source_field)->first();
        $url = $this->getEmbedCode($value);
      }
    }

    return $url;
  }

}
