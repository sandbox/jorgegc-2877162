<?php

namespace Drupal\media_entity_ckan\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media_entity\EmbedCodeValueTrait;

/**
 * Plugin implementation of the 'ckan_resource_embed' formatter.
 *
 * @FieldFormatter(
 *   id = "media_ckan_resource_embed",
 *   label = @Translation("CKAN resource embed"),
 *   field_types = {
 *     "link", "string", "string_long"
 *   }
 * )
 */
class CkanResourceEmbedFormatter extends FormatterBase {

  use EmbedCodeValueTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'width' => 700,
      'height' => 400,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#description' => $this->t('The width of the iframe.'),
      '#default_value' => $this->getSetting('width'),
      '#required' => TRUE,
    ];

    $elements['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#description' => $this->t('The height of the iframe.'),
      '#default_value' => $this->getSetting('height'),
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    if ($this->getSetting('width')) {
      $summary[] = $this->t('Width: @width', [
        '@width' => $this->getSetting('width'),
      ]);
    }

    if ($this->getSetting('height')) {
      $summary[] = $this->t('Height: @height', [
        '@height' => $this->getSetting('height'),
      ]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $embed_code = $this->getEmbedCode($item);
      $embed_url = $this->getEmbedUrl($embed_code);

      if ($embed_url) {
        $element[$delta] = [
          '#theme' => 'media_entity_ckan_resource_embed',
          '#attributes' => [
            'width' => $this->getSetting('width'),
            'height' => $this->getSetting('height'),
            'src' => $embed_url,
            'frameBorder' => '0',
          ],
        ];
      }
    }

    return $element;
  }

  /**
   * Gets the embed URL.
   *
   * @param string $embed_code
   *   The raw embed code.
   *
   * @return string
   *   The embed code URL.
   */
  protected function getEmbedUrl($embed_code) {
    /** @var \Drupal\ckan_connect\Parser\CkanResourceUrlParserInterface $url_parser */
    $url_parser = \Drupal::service('ckan_connect.resource_url_parser');
    return $url_parser->getResourceViewUrl($embed_code);
  }

}
