<?php

namespace Drupal\media_entity_ckan\Plugin\Validation\Constraint;

use Drupal\ckan_connect\Client\CkanClientInterface;
use Drupal\ckan_connect\Parser\CkanResourceUrlParserInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\media_entity\EmbedCodeValueTrait;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the CkanResourceAccessible constraint.
 */
class CkanResourceAccessibleConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  use EmbedCodeValueTrait;

  /**
   * The CKAN client.
   *
   * @var \Drupal\ckan_connect\Client\CkanClientInterface
   */
  protected $ckanClient;

  /**
   * The CKAN resource URL parser.
   *
   * @var \Drupal\ckan_connect\Parser\CkanResourceUrlParserInterface
   */
  protected $ckanResourceUrlParser;

  /**
   * Constructs a new CkanResourceAccessibleConstraintValidator.
   *
   * @param \Drupal\ckan_connect\Client\CkanClientInterface $ckan_client
   *   The CKAN client.
   * @param \Drupal\ckan_connect\Parser\CkanResourceUrlParserInterface $ckan_resource_url_parser
   *   The CKAN resource URL parser.
   */
  public function __construct(CkanClientInterface $ckan_client, CkanResourceUrlParserInterface $ckan_resource_url_parser) {
    $this->ckanClient = $ckan_client;
    $this->ckanResourceUrlParser = $ckan_resource_url_parser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ckan_connect.client'),
      $container->get('ckan_connect.resource_url_parser')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $value = $this->getEmbedCode($value);

    if (empty($value)) {
      return;
    }

    try {
      $params = ['id' => $this->ckanResourceUrlParser->getResourceId($value)];
      $this->ckanClient->get('action/resource_show', $params);
    }
    catch (RequestException $e) {
      $this->context->addViolation($constraint->message);
    }
  }

}
