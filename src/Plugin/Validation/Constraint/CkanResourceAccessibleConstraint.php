<?php

namespace Drupal\media_entity_ckan\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a CKAN resource is accessible.
 *
 * @Constraint(
 *   id = "CkanResourceAccessible",
 *   label = @Translation("CKAN resource publicly acessible", context = "Validation"),
 *   type = { "link", "string", "string_long" }
 * )
 */
class CkanResourceAccessibleConstraint extends Constraint {

  /**
   * The default violation message.
   *
   * @var string
   */
  public $message = 'Referenced CKAN resource is not accessible.';

}
